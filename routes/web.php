<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::resource('login', 'LoginController');

Route::post('login/user', 'LoginController@user');


Route::resource('dashboard', 'DashboardController');

Route::resource('incidents', 'InsidenciaController');

Route::get('incidents/add/{id}', 'InsidenciaController@add');

Route::get('incidents/list/{id}', 'InsidenciaController@list');

Route::resource('solves', 'SolucionController');

Route::get('solves/list/{id}', 'SolucionController@list');