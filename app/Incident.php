<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    protected $fillable = [
       'deparment','worker','title','descript','photo','created_at','updated_at','iduser',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public function incidents()
    {
        return $this->hasMany('App\Solution');
    }
}
