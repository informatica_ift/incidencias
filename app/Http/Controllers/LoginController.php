<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracast\Flash\Flash;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $usuario = User::where('ip',$request->ip) -> first();
        if($usuario != null){
            return view('dashboard')->with('usuario',$usuario);  
            
        }else{
            
            return view('login');    
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::where('id',$id) -> first();
        if($usuario != null){
            return view('dashboard')->with('usuario',$usuario);  
            
        }else{
            flash('La tienda no esta dada de alta, Favor avisar a Departamento de informatica ext: 556 ó 305')->error();
            return view('welcome');    
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user(Request $request)
    {
        $usuario = User::where('email',$request->email) -> first();
        if($usuario != null){
            return view('dashboard')->with('usuario',$usuario);  
            
        }else{
            flash('Favor comunicarce a Departamento de informatica ext: 556 ó 305')->error();
            return view('welcome');    
        }
        
    }
}
