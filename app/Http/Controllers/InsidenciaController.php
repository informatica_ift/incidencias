<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use App\Setting;
use App\User;
use Laracast\Flash\Flash;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class InsidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd($id);
        return view('incidencias.create-incident') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try{

        
        $incident = new Incident($request->all());
        $incident->save();
        
        $usuario = User::where('id',$request->iduser) -> first();
        $incidents = Incident::where('deparment',$usuario->deparment)->get();
        if($incidents != null){
            flash('Incidencia reportada')->success();
            return view('incident')->with('incidents',$incidents)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('incident')->error();    
        }
        }catch(\Exception $e){
            Log::critical($e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage());
            $message = [$e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage()];
            return response()->json($message, 500);
        } 

        
            
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incident = Incident::where('id',$id)->first();
        $usuario = User::where('id',$incident->iduser) -> first();
        return view('incidencias.edit-incident')->with('incident',$incident)->with('usuario',$usuario);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add($id)

    {
        $usuario = User::where('id',$id) -> first();
        $settings = Setting::where('deparment',$usuario->deparment)->get();
        return view('incidencias.create-incident')->with('usuario',$usuario)->with('settings',$settings);  
        
    }

    public function list($id)
    {
        $usuario = User::where('id',$id) -> first();
        $incidents = Incident::where('deparment',$usuario->deparment)->get();
        if($incidents != null){
            return view('incident')->with('incidents',$incidents)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('incident')->error();    
        }
    }


}

