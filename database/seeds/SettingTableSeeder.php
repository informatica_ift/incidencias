<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      foreach (range(1,5) as $index) {
        DB::table('settings')->insert([
            'title' => $faker->catchPhrase,
            'deparment' => $faker->word,
            'priority' => $faker->numberBetween(1,10),
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
        ]);
      }
    }
}
