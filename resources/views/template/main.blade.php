<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'default') | ERP-INCIDENCIAS</title>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset ('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome/css/fontawesome-all.min.css') }}">
        
</head>
<body>
    @include('template.shared.nav')
    <br>
    
    <div class="container">
        <div class="row">
            @yield('content','default')
        </div>
    </div>


<!--script-->
<script src="{{asset ('js/app.js')}} "></script>
@include('template.shared.footer')
</body>
</html>