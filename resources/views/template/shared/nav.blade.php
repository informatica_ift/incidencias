<nav class="navbar navbar-expand-lg" style="background-color: #102747;">
  <a class="navbar-brand" href="#">
    <img src="{{ asset('images/logo.png') }}" class="d-inline-block align-top" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route ('login.show', $usuario->id) }}">Dashboard<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url ('incidents/list', $usuario->id) }}">Incidencias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Soluciones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url ('/') }}"> Logout</a>
      </li>
    
    </ul>
  </div>
</nav>