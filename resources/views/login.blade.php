@extends('template.main2')


@section('title') 
   Bienvenido TabacoBarato
@endsection


@section('content') 
<div class="flex-container top col-12">
    <div class="flex-item">   
        <img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="Responsive image">
    </div>
</div>
<div class="col-12 top-30">
  @include('flash::message')
</div>
<div class="flex-container2 top">
    <div class="flex-item2">
        {{ Form::open(['url' => 'login/user']) }}
            {!!Form::text('email');!!}        
            {!! Form::submit('Ingresar', ['class' => 'btn btn-outline-warning']) !!}      
        {{ Form::close() }}
         
    </div>
</div>
@endsection
