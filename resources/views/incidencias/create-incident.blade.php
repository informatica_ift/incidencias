@extends('template.main')


@section('title') 
  Incidencia
@endsection

@section('content')


{!! Form::open(['route' => 'incidents.store','method' => 'POST', 'files' => false]) !!}
  <div class="form-group">
    <label for="incidencias-lista"><h1>Reporte de Incidencias</h1></label>
    
    <select class="custom-select custom-select-lg mb-3" name="title">
      <option value="NULL">Seleccione una opci&oacute;n</option>
      <option value="Inform&aacute;tica">Inform&aacute;tica y Telecomunicaciones</option>
      <option value="electricidad">Electricidad</option>
      <option value="administracion">Administracion</option>
      <option value="contabilidad">Contabilidad</option>
      <option value="almacen">Almacen</option>
      <option value="compras">Compras</option>
      <option value="RRHH">Recursos Humanos</option>
    </select>
    <label for="descryp">Observaciones:</label>
    <textarea class="form-control" name="descript" rows="3"></textarea>
    <label for="descryp">Firma:</label>
    <input type="text" name="worker" class="form-control">
  </div>
  
  <input type="hidden" value="{{$usuario->id}}" name="iduser">
  <input type="hidden" value="{{$usuario->deparment}}" name="deparment">
  
  <button type="submit" class="btn btn-primary">Reportar</button>
  {!! Form::close() !!}
    
@endsection