@extends('template.main')


@section('title') 
  Incidencia
@endsection

@section('content')
  <div class="form-group col-12">
    <label for="title">Departamento:</label>
    <input type="text" name="title" value="{{$incident->title}}" disabled class="form-control mayuscula">
    <label for="worker">Firma:</label>
    <input type="text" name="worker" value="{{$incident->worker}}" disabled class="form-control mayuscula">
    <label for="created_at">Fecha:</label>
    <input type="text" name="created_at" value="{{$incident->created_at->format('d/m/Y')}}" disabled class="form-control ">
    <label for="descript">Observacion:</label>
    
    <textarea name="descript" rows="3" disabled class="form-control">{{$incident->descript}}</textarea>
    <a href="{{ url ('incidents/list', $usuario->id) }}" class="btn btn-primary top">Volver</a>
  </div>
  
  
    
@endsection