@extends('template.main')


@section('title') 
  Incidencia
@endsection

@section('content') 
<div class="col-12 bottom">
<a href="{{ url ('incidents/add', $usuario->id) }}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="fas fa-address-card"></i>&nbsp;Nueva Incidencia</a>
</div>

<div class="col-12 top-30">
  @include('flash::message')
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>Departamento</th>
      <th>Trabajador</th>
      <th>Dia Publicaci&oacute;n</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach($incidents as $insident)
        <tr>
          <td>{{$insident->title}}</td>
          <td>{{$insident->worker}}</td>
          <td>{{$insident->created_at->format('d/m/Y')}}</td>
          <td>
              <a href="{{ route ('incidents.show',$insident->id) }}"><i class="fas fa-eye"></i></a> 
          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
</table>
</div>
    
@endsection