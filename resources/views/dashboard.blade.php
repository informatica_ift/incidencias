@extends('template.main')


@section('title') 
   Dashboard | ERP-Insidensias
@endsection

@section('content') 
<div class="card col-12 ">
  <div class="card-body">
    Tienda: {{$usuario->deparment}}<br/>
    Nombre Encargada: {{$usuario->name}}<br/>
    Correo contacto: {{$usuario->email}}<br/>
  </div>
</div>
<div class="card top espacio" style="width: 10rem; height:10rem">
    <a href="{{ url ('incidents/add', $usuario->id) }}">
        <img class="card-img-top" src="{{ asset('images/insidensias.png') }}">
    </a>    
</div>

<div class="card top espacio" style="width: 10rem; height:10rem">
<a href="#">
    <img class="card-img-top" src="{{ asset('images/correo.jpg') }}" alt="reporte insidencia">
</a>  
  
</div>

<div class="card top espacio" style="width: 10rem; height:10rem">
<a href="#">
    <img class="card-img-top" src="{{ asset('images/soluciones.jpg') }}" alt="reporte insidencia">
</a>  
  
</div>

@endsection